var model = require("../models/index");
    module.exports = function(app) {
    /* GET Book listing. */
    app.get("/books", function(req, res, next) {
        model.Book.findAll({})
            .then(book =>
            res.json({
                error: false,
                data: book
            })
        )   
        .catch(error =>
        res.json({
            error: true,
            data: [],
            error: error
            })
        );
});
/* GET Book listing. */
app.get("/books/:id", function(req, res, next) {
    const book_id = req.params.id;
    model.Book.findAll(
        {
            where: {
                id: book_id,
            }
        }
        )
        .then(book =>
        res.json({
            error: false,
            data: book
        })
    )   
    .catch(error =>
    res.json({
        error: true,
        data: [],
        error: error
        })
    );
});

/* POST todo. */
app.post("/books", function(req, res, next) {
    const { title, author, published_date, pages, language, publisher_id } = req.body;
        model.Book.create({
            title: title,
            author: author,
            published_date: published_date,
            pages: pages,
            language: language,
            publisher_id: publisher_id
        })
        .then(book =>
        res.status(201).json({
            error: false,
            data: book,
            message: "New book has been created."
            })
        )
        .catch(error =>
        res.json({
            error: true,
            data: [],
            error: error
            })
        );
});

/* update Todo. */
app.put("/books/:id", function(req, res, next) {
    const book_id = req.params.id;
    const { title, author, published_date, pages, language, publisher_id } = req.body;
        model.Book.update(
        {
            title: title,
            author: author,
            published_date: published_date,
            pages: pages,
            language: language,
            publisher_id: publisher_id

        },
        {
            where: {
            id: book_id
        }
        }
    )
        .then(book =>
         res.json({
            error: false,
            message: "book has been updated."
        })
    )
        .catch(error =>
         res.json({
            error: true,
            error: error
        })
    );
});

/* Delete todo. */
app.delete("/books/:id", function(req, res, next) {
    const book_id = req.params.id;
        model.Book.destroy({
            where: {
            id: book_id
        }
        })
            .then(status =>
             res.json({
                error: false,
                message: "todo has been delete."
            })
        )
            .catch(error =>
             res.json({
                error: true,
                error: error
            })
        );
    });
};