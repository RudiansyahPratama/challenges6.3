'use strict';
module.exports = (sequelize, DataTypes) => {
  const Book = sequelize.define('Book', {
    title: {
      type: DataTypes.STRING, 
      allowNull: false,
      validate: { min: 4, max: 90 }
      },
    author: [
      {
     type: DataTypes.STRING
    }
  ],
    published_date: {
      type: DataTypes.DATEONLY,
        validate: {
          isDate: true,
        }
    },
    pages:{ 
     type: DataTypes.INTEGER
    },
    language: {
     type: DataTypes.STRING 
    },
    publisher_id:{
      type: DataTypes.STRING},

  }, {});
  Book.associate = function(models) {
    // associations can be defined here
  };
  return Book;
};