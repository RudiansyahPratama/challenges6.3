const express = require("express");
const path = require("path");
const logger = require("morgan");
const bodyParser = require("body-parser");

const app = express();

app.use(logger("dev"));
app.use(bodyParser.json());

require("./routes/route.js")(app);
// Create a Server
var server = app.listen(3000, "127.0.0.1", function() {
var host = server.address().address;
var port = server.address().port;
console.log("App listening at http://%s:%s", host, port);
});